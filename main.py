import json

User_session_now = {}


def main():
    db_name = "data.json"
    load_db(db_name)
    users_info_back, users_in_game_info = get_bd_data(db_name)
    _start_process(db_name, users_info_back, users_in_game_info)


def get_bd_data(db_name):
    with open(db_name, "r") as db:
        data = json.loads(db.read())
        db.close()
    users_info_back = data["users_info_back"]
    users_in_game_info = data["users_in_game_info"]
    return users_info_back, users_in_game_info


def load_db(db_name):
    status = True
    try:
        with open(db_name, 'r') as db:
            json.load(db)
            db.close()
        status = False
    except Exception as er1:
        print(f"Ошибка запуска json {er1}")
        while True:
            z = str(input("Хотите создать новый .json?(Y/N) "))
            if z == "N":
                exit(0)
            elif z == "Y":
                break
            else:
                print("Чтобы запустить программу создайте .json")
    if status:
        users_info_back = []
        users_in_game_info = []
        try:
            with open(db_name, 'w') as db:
                db.write(json.dumps({"users_info_back": users_info_back,
                                     "users_in_game_info": users_in_game_info},
                                    indent=4))
                db.close()
        except Exception as er2:
            print(f"При загрузке db произошла ошибка {er2}")
            exit(0)
    print("db загружена")


def save_db(db_name, users_info_back, users_in_game_info):
    try:
        with open(db_name, 'w') as db:
            db.write(json.dumps({"users_info_back": users_info_back,
                                 "users_in_game_info": users_in_game_info},
                                indent=4))
            db.close()
        print(True)
    except Exception as e1:
        print(False, e1)


def _start_process(db_name, users_info_back, users_in_game_info):
    global User_session_now
    game = True
    while game:
        print("Выберете действие.")
        print("Введите 1 зарегестрироваться")
        print("Введите 2 войти в аккаунт")
        print("Введите qq чтобы выйти из приложения")
        var = str(input("Ваше действие: "))
        if var in ['1', '2', 'qq']:
            if var == '1':
                if reg_user(db_name, users_info_back, users_in_game_info):
                    game = False
                else:
                    User_session_now = {}
            elif var == '2':
                if log_user(users_info_back):
                    game = False
                else:
                    User_session_now = {}
            else:
                print('bb')
                exit(0)
        else:
            print("Действие не опознано")

    gaming(db_name, users_info_back, users_in_game_info)


def gaming(db_name, users_info_back, users_in_game_info):
    global User_session_now
    while True:
        print("Выберете действие.")
        print("Введите 1 просмотреть информацию о своём профиле")
        print("Введите 2 просмотреть информацию об игроввом аккаунте")
        print("Введите 3 чтобы изменить информоцию о профиле")
        print("Введите qq чтобы выйти из приложения")
        var = str(input("Ваше действие: "))
        if var in ['1', '2', '3', 'qq']:
            if var == '1':
                view_user_back_info()
            elif var == '2':
                view_users_in_game_info(users_in_game_info)
            elif var == '3':
                user_back_info_hss(db_name,
                                   users_info_back, users_in_game_info)
            else:
                print('bb')
                exit(0)
        else:
            print("Действие не опознано")


def user_back_info_hss(db_name, users_info_back, users_in_game_info):
    print("Выберете действие.")
    print("Введите 1 поменять имя")
    print("Введите 2 поменять пароль")
    print("Введите qq чтобы выйти в главное меню")
    var = str(input("Ваше действие: "))
    if var in ['1', '2', 'qq']:
        if var == '1':
            password_ch_user_back_info(db_name, users_info_back,
                                       users_in_game_info)
        elif var == '2':
            name_ch_user_back_info(db_name, users_info_back,
                                   users_in_game_info)
        else:
            print('bb')
            exit(0)
    else:
        print("Действие не опознано")


def password_ch_user_back_info(db_name, users_info_back, users_in_game_info):
    global User_session_now
    while True:
        print("Введите имя для смены")
        login = str(input())
        if login not in [x["login"] for x in users_info_back]:
            users_info_back[users_info_back.index(User_session_now)]["login"] \
                = login
            save_db(db_name, users_info_back, users_in_game_info)
            return
        else:
            print("Имя занято")


def name_ch_user_back_info(db_name, users_info_back, users_in_game_info):
    global User_session_now
    print("Введите пароль для смены")
    password = str(input())
    users_info_back[users_info_back.index(User_session_now)]["password"] \
        = password
    save_db(db_name, users_info_back, users_in_game_info)
    return


def view_user_back_info():
    global User_session_now
    print(User_session_now)


def view_users_in_game_info(users_in_game_info):
    global User_session_now
    print(filter(lambda x: x["user_id"] == User_session_now["id"],
                 users_in_game_info))


def reg_user(db_name, users_info_back, users_in_game_info):
    global User_session_now
    print("Чтобы выйти в главное меню введите 'qq'")
    user_id = len(users_info_back) + 1
    while True:

        login = str(input("Введите ваше имя: "))
        password = str(input("Введите пароль: "))
        if login == 'qq' or password == 'qq':
            return False
        elif login in [x["login"] for x in users_info_back]:
            print("имя занято")
        else:
            break
    users_info_back.append({"id": user_id, "login": login,
                            "password": password})
    users_in_game_info.append({"user_id": user_id, "max_hp": 100,
                               "max_mp": 10, "max_st_at": 35, "bp": []})
    save_db(db_name, users_info_back, users_in_game_info)
    User_session_now = {"id": user_id, "login": login,
                        "password": password}
    return True


def log_user(users_info_back):
    global User_session_now
    print("Чтобы выйти в главное меню введите 'qq'")
    print("Введите ваш login ")
    while True:
        login = str(input())
        if login == 'qq':
            print("Выход в главное меню")
            return False
        elif login in [x["login"] for x in users_info_back]:
            print("Пользователь найден")
            User_session_now = list(filter(lambda x: x["login"] == login,
                                           users_info_back))[0]
            if validate_user_pass():
                return True
            return False

        else:
            print("Такого пользователя нет")


def validate_user_pass():
    global User_session_now
    print("Чтобы выйти в главное меню введите 'qq'")
    print("Введите ваш password")
    while True:
        password = str(input())
        if password == 'qq':
            print("Выход в главное меню")
            return False
        if password == User_session_now["password"]:
            print("Вы вошли в аккаунт")
            return True
        else:
            print("непраильный пароль")


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print(e)
